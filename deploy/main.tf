terraform {
  backend "s3" {
    bucket         = "receipe-app-api-devops-state"
    key            = "receipe-app.tfstate"
    region         = "eu-west-3"
    encrypt        = true
    dynamodb_table = "receipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-3"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environement = terraform.workspace
    Project      = var.project
    Owner        = var.contact
    ManagedBy    = "Terraform"
  }
}
