variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "receipe-app-api-devops"
}

variable "contact" {
  default = "barnabe.202108@gmail.com"
}
